<?php

$MOCK_DATA = array();

$MOCK_DATA['content'] = array(
  0 => array(
    "id" => 1,
    "order" => 1,
    "slug" => "home",
    "options" => '{"title":"We Are Code Cause"}'
  ),
  1 => array(
    "id" => 2,
    "order" => 2,
    "slug" => "home_boxes_1",
    "options" => '{"text":"This is content in box #1", "button_text":"Button #1", "button_href":"#location"}'
  ),
  2 => array(
    "id" => 3,
    "order" => 3,
    "slug" => "home_boxes_2",
    "options" => '{"text":"This is content in box #2", "button_text":"Button #2", "button_href":"#location"}'
  ),
  3 => array(
    "id" => 4,
    "order" => 4,
    "slug" => "home_boxes_3",
    "options" => '{"text":"This is content in box #3", "button_text":"Button #3", "button_href":"#location"}'
  ),
  4 => array(
    "id" => 5,
    "order" => 5,
    "slug" => "people",
    "options" => '{"id": "people", "title":"Who We Are", "text":"This is content in the people section"}'
  ),
  5 => array(
    "id" => 6,
    "order" => 6,
    "slug" => "bio_1",
    "options" => '{"name": "Tom Kolby", "title":"Founder & CEO", "bio":"Tom Bio", "image":"url"}'
  ),
  6 => array(
    "id" => 7,
    "order" => 7,
    "slug" => "bio_2",
    "options" => '{"name": "Keegan Myers", "title":"Founder & CTO", "bio":"Keegan Bio", "image":"url"}'
  ),
  7 => array(
    "id" => 8,
    "order" => 8,
    "slug" => "work",
    "options" => '{"id": "work", "title":"What We Offer", "text":"This is content in the work section", "button_text":"Work Button", "button_href":"#location", "image":"url"}'
  ),
  8 => array(
    "id" => 9,
    "order" => 9,
    "slug" => "contact",
    "options" => '{"id": "contact", "title":"We Work For You", "text":"This is content in the contact section"}'
  )
);

$MOCK_DATA['applications'] = array(
  0 => array(
    "id" => 1,
    "status" => "Denied",
    "date" => "07/19/2014",
    "ip" => "109.236.092",
    "options" => '{"Organization Name":"Monsanto","Project Description":"New website for our GMO project"}'
  ),
  1 => array(
    "id" => 2,
    "status" => "Received",
    "date" => "05/05/2014",
    "ip" => "678.123.672",
    "options" => '{"Organization Name":"Arch Grants","Project Description":"New mobile app for automatically investing in the best St Louis startups"}'
  )
);

$MOCK_DATA['users'] = array(
  0 => array(
    "id" => 1,
    "email" => "user",
    "password" => password_hash('password', PASSWORD_DEFAULT)
  )
);

?>