#The life of a request

Such as GET /admin/content...

* .htaccess file directs all requests to /index.php

* index.php breaks down the request and figures out the $slug, in this case, 'content'

* since this is not a POST request, index.php calls page.php and inserts the proper which is content.php ($slug.php) it also calls the proper functions to be loaded before this page which is function/$slug.php



Such as POST /submit/applications

* .htaccess file directs all requests to /index.php

* index.php breaks down the request and figures out the $slug, in this case, 'applications'

* since this is a POST request, index.php calls post_requests.php which handles the request. index.php also calls the proper functions to be loaded before the post_requests.php which is functions/$slug.php