<hr>

<?php foreach (getAllApplications() as $application) { ?>

<div class="applications" data-id="<?php echo $application['id']; ?>">
      
<?php foreach (json_decode($application['options']) as $option_name => $option_value) { ?>

  <div class="key-value-pair">
    <label><?php echo $option_name; ?></label>
    <label><?php echo $option_value; ?></label>
  </div>

<?php } // end $options foreach ?>
        
  <br>

  <div class="key-value-pair">
    <label>Date Submitted</label>
    <label><?php echo $application['date_submitted']; ?></label>
  </div>

  <div class="key-value-pair">
    <label>IP Address</label>
    <label><?php echo $application['ip']; ?></label>
  </div>

  <br>

  <div class="row">
    <div class="large-3 columns">
      <label class="status">Application Status</label>
    </div>
    
    <div class="large-3 columns left">
      <select>
        <?php // ternary operator.. kinda cool.. check it out :) ?>
        <option <?php echo $application['status'] == "Received" ? "selected" : ""; ?>>Received</option>
        <option <?php echo $application['status'] == "Reviewed" ? "selected" : ""; ?>>Reviewed</option>
        <option <?php echo $application['status'] == "Denied" ? "selected" : ""; ?>>Denied</option>
        <option <?php echo $application['status'] == "Accepted" ? "selected" : ""; ?>>Accepted</option>
      </select>
    </div>
  </div>

  <br>

  <button class="small button update-application">Update</button>
  <button class="small alert button delete-application">Delete</button>

  <hr>

</div>
  
<?php } // end $applications foreach ?>


<h3>Usage</h3>
<code>POST Request to /admin/submit/applications</code>
<br><br>
<h4>Example</h4>
<p>
  <strong>Code</strong>
  <br>
  <code>var data_fields = {"fields":<br>{"Organization Name":"Arch Grants",<br>"Project Description":"New mobile app for automatically investing in the best St Louis startups"}<br>};
    <br><br>
    $.post("/submit/applications", data_fields);
  </code>
</p>

<hr>