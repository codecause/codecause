<?php

include('admin/functions/functions.php');

// see documenation in admin for usage
function getContent($slug, $option_key) {
  $options = queryDatabase("SELECT options FROM content WHERE slug = ?", array($slug));

  echo json_decode($options[0]['options'], true)[$option_key];
}

?>