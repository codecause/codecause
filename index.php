<?php include('functions.php'); ?>

<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Code Cause</title>

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/app.css">
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Geo|Open+Sans:300italic,400,300,600|Press+Start+2P">
  
  <!-- For iPhone 4 -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/h/apple-touch-icon.png">
  <!-- For iPad 1-->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/m/apple-touch-icon.png">
  <!-- For iPhone 3G, iPod Touch and Android -->
  <link rel="apple-touch-icon-precomposed" href="img/l/apple-touch-icon-precomposed.png">
  <!-- For Nokia -->
  <link rel="shortcut icon" href="img/l/apple-touch-icon.png">
  <!-- For everything else -->
  <link rel="shortcut icon" href="http://codecause.org/favicon.ico">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon"> 

  <script src="js/vendor/modernizr.js"></script>
</head>
  
<body>
  <div class="contain-to-grid sticky">
    <nav class="top-bar" data-topbar>

      <ul class="title-area">
      	<div class="row">
      		<div class="large-12 medium-12 small-6 small-centered columns">
        		<li class="name"><h1><a href="#"><img class="size" src="img/cc_logo.png" alt="Code Cause" /></a></h1></li>
        	</div>
        </div>		
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
      </ul>

      <section class="top-bar-section">
        <ul class="right">
          <li><a href="#">Home</a></li>
          <li><a href="#people">People</a></li>
   <!--       <li><a href="#work">Work</a></li> -->
          <li><a href="#sponsors">Sponsors</a></li>
          <li><a href="#partners">Partners</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </section>

    </nav>
  </div>
  
  <!-- Home Section -->
  <div class="header home">
    <div class="row">
      <div class="large-12 medium-12 columns">
        <h1><?php getContent('home', 'title'); ?></h1>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 medium-1 small-1 small-centered medium-centered large-centered columns">
      <img class="lift" src="img/heart.png">
    </div>
  </div>

  <div class="row">
    <div class="large-12 medium-12 small-12 columns">
      <p><?php getContent('home_content'); ?></p>
    </div>
  </div>

  <div class="row">
  	<div class="large-4 medium-4 small-12 columns">
      	<p class="hero"><?php getContent('home_boxes_1', 'text'); ?></p>
      	<div class="row">
      		<div class="large-6 medium-6 large-centered medium-centered small-12 columns">
      			<a href="<?php getContent('home_boxes_1', 'button_href'); ?>" class="button small expand radius"><?php getContent('home_boxes_1', 'button_text'); ?></a>
    		</div>
    	</div>	
    </div>

    <div class="large-4 medium-4 small-12 columns">
      	<p class="hero"><?php getContent('home_boxes_2', 'text'); ?></p>
	  	<div class="row">
	  		<div class="large-6 medium-6 large-centered medium-centered small-12 columns">
	  			<a href="<?php getContent('home_boxes_2', 'button_href'); ?>" class="button small expand radius"><?php getContent('home_boxes_2', 'button_text'); ?></a>
			</div>
		</div>		
    </div>

    <div class="large-4 medium-4 small-12 columns">
    	<p class="hero"><?php getContent('home_boxes_3', 'text'); ?></p>
      	<div class="row">
      		<div class="large-6 medium-6 large-centered medium-centered small-12 columns">
      			<a href="<?php getContent('home_boxes_3', 'button_href'); ?>" class="button small expand radius"><?php getContent('home_boxes_3', 'button_text'); ?></a>
      		</div>
      	</div>	 	
    </div>
  </div>
  <!-- End Home Section -->

  <!-- People Section -->
  <div id="<?php getContent('people', 'id'); ?>"></div>
  <div class="header people">
  	 <div class="row">
  		<div class="large-12 medium-12 columns">
        	<h1><?php getContent('people', 'title'); ?></h1>
    	</div>
  	 </div>
  </div>
  
  <div class="row">
    <div class="large-1 medium-1 small-1 small-centered medium-centered large-centered columns">
      <img class="lift" src="img/heart.png">
    </div>
  </div>

  <div class="row">
    <div class="large-12 medium-12 small-12 columns">
      <h4><?php getContent('people', 'mission'); ?></h4>
      <p><?php getContent('people', 'solutions'); ?></p>
        <br /><br />
    </div>
  </div>

  <div class="row">
    <div class="large-6 columns">
      <div class="large-4 medium-4 columns">
        <img src="<?php getContent('bio_1', 'image'); ?>" alt="" />
      </div>
      <div class="large-8 medium-8 columns">
        <h4><?php getContent('bio_1', 'name'); ?></h4>
        <h5><?php getContent('bio_1', 'title'); ?></h5>
        <p><?php getContent('bio_1', 'bio'); ?></p>
      </div>
    </div>
    <div class="large-6 columns">
      <div class="large-4 medium-4 columns">
        <img src="<?php getContent('bio_2', 'image'); ?>" alt="" />
      </div>
      <div class="large-8 medium-8 columns">
        <h4><?php getContent('bio_2', 'name'); ?></h4>
        <h5><?php getContent('bio_2', 'title'); ?></h5>
        <p><?php getContent('bio_2', 'bio'); ?></p>
      </div>
    </div>
  </div>
  <!-- End People Section -->

  <!-- Work Section 
  <div id="<?php getContent('work', 'id'); ?>"></div>
  <div class="header work"> 
    <div class="row">
      <div class="large-12 medium-12 small-12 columns">
        <h1><?php getContent('work', 'title'); ?></h1>
      </div>
    </div>
  </div>  
  
  <div class="row"> 
    <div class="large-1 medium-1 small-1 small-centered medium-centered large-centered columns">
      <img class="lift" src="img/heart.png" alt="" />
    </div>
  </div>

  <div class="row sink">
    <div class="large-6 medium-6 columns hide-for-small-only">
      <img src="<?php getContent('work', 'image'); ?>" alt="" />
    </div>
    <div class="large-6 medium-6 small-12 columns">
      <p><?php getContent('work', 'text'); ?></p>
      <a class=" small button radius" href="<?php getContent('work', 'button_href'); ?>"><?php getContent('work', 'button_text'); ?></a>
    </div>
  </div>
   End Work Section -->
  
  <!-- Sponsor Section -->
  <div id="<?php getContent('sponsors', 'id'); ?>"></div>
    <div class="header sponsor"> 
      <div class="row">
        <div class="large-12 medium-12 small-12 columns">
          <h1><?php getContent('sponsors', 'title'); ?></h1>
        </div>
      </div>
    </div>  
    
    <div class="row"> 
      <div class="large-1 medium-1 small-1 small-centered medium-centered large-centered columns">
        <img class="lift" src="img/heart.png" alt="" />
      </div>
    </div>
    <div class="row sink">
      	<div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('sponsors', 'image1_href'); ?>"><img src="<?php getContent('sponsors', 'image1'); ?>"/></a>
        </div>
        <div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('sponsors', 'image2_href'); ?>"><img src="<?php getContent('sponsors', 'image2'); ?>"/></a>
        </div>
        <div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('sponsors', 'image3_href'); ?>"><img src="<?php getContent('sponsors', 'image3'); ?>"/></a>
        </div>
      </div> 
     <!-- <div class="row sink">
      	<div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('sponsors', 'image4_href'); ?>"><img src="<?php getContent('sponsors', 'image4'); ?>"/></a>
        </div>
        <div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('sponsors', 'image5_href'); ?>"><img src="<?php getContent('sponsors', 'image5'); ?>"/></a>
        </div>
        <div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('sponsors', 'image6_href'); ?>"><img src="<?php getContent('sponsors', 'image6'); ?>"/></a>
        </div>
        <div class="large-6 medium-6 small-12 columns">
            <p><?php getContent('sponsors', 'text_leftcolumn'); ?></p>
        </div>
        <div class="large-6 medium-6 small-12 columns">
            <p><?php getContent('sponsors', 'text_rightcolumn'); ?></p>
                <a class="small button radius" href="<?php getContent('sponsors', 'button_href'); ?>"><?php getContent('sponsors', 'button_text'); ?></a>
        </div>
       </div>
    <!-- sponsor Section -->
    
    <!-- partners Section -->
      <div id="<?php getContent('partners', 'id'); ?>"></div>
      <div class="header partners"> 
        <div class="row">
          <div class="large-12 medium-12 small-12 columns">
            <h1><?php getContent('partners', 'title'); ?></h1>
          </div>
        </div>
      </div>  
      
      <div class="row"> 
        <div class="large-1 medium-1 small-1 small-centered medium-centered large-centered columns">
          <img class="lift" src="img/heart.png" alt="" />
        </div>
      </div>
      <div class="row sink">
      	<div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('partners', 'image1_href'); ?>"><img src="<?php getContent('partners', 'image1'); ?>"/></a>
            <p><?php getContent('partners', 'image1_text'); ?></p>
        </div>
        <div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('partners', 'image2_href'); ?>"><img src="<?php getContent('partners', 'image2'); ?>"/></a>
            <p><?php getContent('partners', 'image2_text'); ?></p>
        </div>
        <div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('partners', 'image3_href'); ?>"><img src="<?php getContent('partners', 'image3'); ?>"/></a>
            <p><?php getContent('partners', 'image3_text'); ?></p>
        </div>
      </div> 
<!--      <div class="row sink">
      	<div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('partners', 'image4_href'); ?>"><img src="<?php getContent('partners', 'image4'); ?>"/></a>
            <p><?php getContent('partners', 'image4_text'); ?></p>
        </div>
        <div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('partners', 'image5_href'); ?>"><img src="<?php getContent('partners', 'image5'); ?>"/></a>
            <p><?php getContent('partners', 'image5_text'); ?></p>
        </div>
        <div class="large-4 medium-4 small-12 columns">
          <a href="<?php getContent('partners', 'image6_href'); ?>"><img src="<?php getContent('partners', 'image6'); ?>"/></a>
            <p><?php getContent('partners', 'image6_text'); ?></p>
        </div>-->
         <div class="large-6 medium-6 small-12 columns">
            <p><?php getContent('partners', 'text_leftcolumn'); ?></p>
        </div>
        <div class="large-6 medium-6 small-12 columns">
            <p><?php getContent('partners', 'text_rightcolumn'); ?></p>
        </div>
      <!--partners Section -->
  

  <!-- Contact Section -->
  <div id="<?php getContent('contact', 'id'); ?>"></div>
  <div class="header contact">
    <div class="row">
      <div class="large-12 medium-12 small-12 columns">
        <h1><?php getContent('contact', 'title'); ?></h1>
      </div>
    </div>
  </div>

  <div class="row"> 
    <div class="large-1 medium-1 small-1 small-centered medium-centered large-centered columns">
      <img class="lift" src="img/heart.png" alt="" />
    </div>
  </div>

  <div class="row sink">
    <div class="large-6 medium-6 small-12 columns">             
      <p><?php getContent('contact', 'text'); ?></p>
        <p>(314)258-1111</p>
        <p>1500 Washington Avenue</p>
        <p>St. Louis, MO 63103</p>
    </div>

    <div class="large-6 medium-6 small-12 columns">
      <form method="POST" action="">
        <input type="text" name="Organization Name" placeholder="Organization Name">
        <input type="text" name="Email" placeholder="Email">
        <input type="text" name="Phone Number" placeholder="Phone Number">
        <textarea name="Project Description" placeholder="Short Project Description" style="resize: vertical;"></textarea>
        <input type="submit" class="small button radius" value="Submit" />
      </form>
    </div>
  </div>

  <div class="space"></div>
  
  <div class="footer">
    <div class="row">
        <div class="large-12 medium-12 small-12 columns">
            <p>CodeCause &copy; 2014 - Photography by <a href="http://www.nathanlucy.com">Nathan Lucy</a></p>
      </div>
    </div>
  </div>


  <script src="js/vendor/jquery.js"></script>
  <script src="js/vendor/foundation.min.js"></script>
  <script>$(document).foundation();</script>
  <script src="js/vendor/fastclick.js"></script>
  <script src="js/vendor/placeholder.js"></script>
  <script src="js/app.js"></script>
  <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55312646-1', 'auto');
        ga('send', 'pageview');

</script>

</body>
</html>
